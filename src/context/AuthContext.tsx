import React, { createContext, useReducer } from 'react';
import { authReducer } from './authReducer';

// Definir como luce, o qué información tendré aquí
export interface AuthState {
  isLoggedIn: boolean;
  username?: string;
  favoriteIcon? : string;
}

// Estado inicial
export const authInitialState: AuthState = {
  isLoggedIn: false,
};

// Lo usaremos para decirla a react cómo luce y que expone el context
export interface AuthContextProps {
  authState: AuthState;
  signIn: () => void;
  signOut: () => void;
  changeFavoriteIcon: (_: string) => void;
  changeUsername: (_: string) => void;
}

// crear el contexto
export const AuthContext = createContext({} as AuthContextProps);

// Componente del proveedor del estado
export const AuthProvider = ({ children }: any) => {

  const [authState, dispatch] = useReducer(authReducer, authInitialState);

  const signIn = () => {
    dispatch({ type: 'signIn' });
  };

  const signOut = () => {
    dispatch({ type: 'signOut' });
  };

  const changeFavoriteIcon = (icoName: string) => {
    dispatch({ type: 'changeFavIcon', payload: icoName });
  };

  const changeUsername = (username: string) => {
    dispatch({ type: 'changeUsername', payload: username });
  };

  return (
    <AuthContext.Provider value={{
      authState,
      signIn,
      signOut,
      changeFavoriteIcon,
      changeUsername,
    }}>
      { children }
    </AuthContext.Provider>
  );
};
