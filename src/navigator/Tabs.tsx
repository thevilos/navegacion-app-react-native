import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { Tab1Screen } from '../screens/Tab1Screen';
import { StackNavigator } from './StackNavigator';
import { colores } from '../theme/appTheme';
import { Platform, Text } from 'react-native';
import { TopTab } from './TopTab';


export const Tabs = () => {

  return Platform.OS === 'ios' ? <TabsIOS /> : <TabsAndroid />;
};

const BottomTabAndroid = createMaterialBottomTabNavigator();

export const TabsAndroid = () => {
  return (
    <BottomTabAndroid.Navigator
      sceneAnimationEnabled={true}
      barStyle={{
        backgroundColor: colores.primary,
      }}
      screenOptions={ ({ route }) => ({
        tabBarActiveTintColor: colores.primary,
        tabBarStyle: {
            borderTopColor: 'red',
            borderTopWidth: 0,
            elevation: 0,
        },
        tabBarLabelStyle: {
            fontSize: 15,
        },
        tabBarIcon: (props) => {
            let iconName: string = '';
            switch (route.name) {
                case 'Tab1Screen':
                  iconName = 'T1';
                break;

                case 'Tab2Screen':
                  iconName = 'T2';
                break;

                case 'StackNavigator':
                  iconName = 'ST';
                break;
            }
            return <Text style={{color: props.color}}>{ iconName }</Text>;
        },
      })}
    >
      <BottomTabAndroid.Screen name="Tab1Screen" options={{ title: 'Tab 1' }} component={Tab1Screen} />
      <BottomTabAndroid.Screen name="Tab2Screen" options={{ title: 'Tab 2' }} component={TopTab} />
      <BottomTabAndroid.Screen name="StackNavigator" options={{ title: 'Stack' }} component={StackNavigator} />
    </BottomTabAndroid.Navigator>
  );
};


const BottomTabIOS = createBottomTabNavigator();

export const TabsIOS = () => {
  return (
    <BottomTabIOS.Navigator
      sceneContainerStyle={{
        backgroundColor: 'white',
      }}
      screenOptions={ ({ route }) => ({
        tabBarActiveTintColor: colores.primary,
        tabBarStyle: {
            borderTopColor: 'red',
            borderTopWidth: 0,
            elevation: 0,
        },
        tabBarLabelStyle: {
            fontSize: 15,
        },
        tabBarIcon: (props) => {
            let iconName: string = '';
            switch (route.name) {
                case 'Tab1Screen':
                  iconName = 'T1';
                break;

                case 'Tab2Screen':
                  iconName = 'T2';
                break;

                case 'StackNavigator':
                  iconName = 'ST';
                break;
            }
            return <Text style={{color: props.color}}>{ iconName }</Text>;
        },
      })}
    >
      {/* <Tab.Screen name="Tab1Screen" options={{ title: 'Tab 1', tabBarIcon: (props) => <Text style={{ color: props.color }}>T1</Text> }} component={Tab1Screen} /> */}
      <BottomTabIOS.Screen name="Tab1Screen" options={{ title: 'Tab 1' }} component={Tab1Screen} />
      <BottomTabIOS.Screen name="Tab2Screen" options={{ title: 'Tab 2' }} component={TopTab} />
      <BottomTabIOS.Screen name="StackNavigator" options={{ title: 'Stack' }} component={StackNavigator} />
    </BottomTabIOS.Navigator>
  );
};
